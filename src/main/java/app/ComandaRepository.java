package app;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface ComandaRepository   extends CrudRepository<Comanda, Integer>{

	 Optional<Comanda> findByIdComanda(int idcomanda);

	
}

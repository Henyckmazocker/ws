package app;

import org.springframework.data.repository.CrudRepository;

public interface LotRepository extends CrudRepository<Lot, Integer>{

}

package app;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;


public interface ProducteRepository  extends CrudRepository<Producte, Integer>{

	 Optional<Producte> findByNomProducteIgnoreCase(String nomProducte);

	 //List<Lot> findbyProducteOrderByPreuIgnoreCase(Optional<Producte> producte);
	 
}

package app;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller    // This means that this class is a Controller
@RequestMapping(path="/lot") // This means URL's start with /demo (after Application path)
public class LotController {
	@Autowired // This means to get the bean called userRepository
	           // Which is auto-generated by Spring, we will use it to handle the data
	private LotRepository LotRepository;
	@Autowired 
	private ProducteRepository PR;
	@GetMapping(path="/add") // Map ONLY GET Requests
	public @ResponseBody String addNewLot (@RequestParam int quantitat
			, @RequestParam Date dataentrada,@RequestParam Date datacaducitat, @RequestParam int idproduct) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		
		Lot n = new Lot();
		n.setQuantitat(quantitat);;
		n.setDataEntrada(dataentrada);
		n.setDataCaducitat(datacaducitat);
		n.setProducte(PR.findById(idproduct).get());
		LotRepository.save(n);
		return "Saved";
	}
	
	@GetMapping(path="delete{id}")
	public @ResponseBody String deleteLot(@RequestParam int id) {
		
		LotRepository.deleteById(id);
		
		return "Borrado";

	}
	
	@GetMapping(path="find/{id}")
	public @ResponseBody Lot getLot(@PathVariable int id) {
		
		Optional<Lot> lot = LotRepository.findById(id);

		return lot.get();
	}

	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<Lot> getAllLots() {
		// This returns a JSON or XML with the users
		return LotRepository.findAll();
	}
}
